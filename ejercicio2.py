#!/usr/bin/env python
# -*- coding: utf-8 -*-

# función creadora de traingulo
# trae altura ingreasada por usuario
def crear_triangulo(altura):

    # ciclo crea y recorre matriz vacía

    for i in range(altura):
        matriz.append([])

        # se duplica el ancho de la matriz para formar un triangulo

        for j in range(altura*2):

            # para que haya un espacio vaciío
            # j sebe ser mayor que i
            # también menor al doble de la altura
            # menos i -1 para que se forme una
            # diagonal por el lado opuesto 

            if j > i and j < (altura * 2 - i - 1):
                matriz[i].append(' ')

            # si no es así, se agregará un '*'
            # con j menor o igual a i, por ejemplo

            else:
                matriz[i].append('*')

    # se imprime la matriz con espacios
    for i in range(altura):
        print(' '.join(matriz[i]))

# variables
# altura y matriz vacía
altura = int(input("Ingresar altura triangulo: "))
matriz = []

# se llama la función
crear_triangulo(altura)
