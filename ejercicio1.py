#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

# función para invertir la lista
# usando .reverse
def lista_invertida(lista):
    lista.reverse()
    print ("Lista inversa:",lista)

# función crea primera lista aleatoria
def lista_1(frase, lista, largo):

    # ciclo crea una lista del mismo tamaño
    # que la lista original

    for i in range(largo):

        # aleatoriamente se le agrega un valor a la lista
        # proveniente de la lista original

        lista.append(random.choice(frase))
        # ~ print(lista[i])
        repeticion = lista.count(lista[i])

        # entrará en un ciclo si hay una palabra 
        # repetida en la lista
        # si existe una repitición
        # esta se cambiará por otra palabra de la
        # lista original
        # y seguirá así hasta que no haya ninguna repetida

        while repeticion >= 2:
            repeticion = 0
            # ~ print(lista[i], "repetido")
            lista[i] = random.choice(frase)
            repeticion = lista.count(lista[i])
                # ~ print(lista[i], "cambio")

    # imprime lista aleatoria
    print("Lista aleatoria:", lista)


# variables
# lista original
# listas vacías y el largo de la original
frase = ['Di', 'buen', 'día', 'a', 'papa']
lista_temp = []
lista = []
largo = (len(frase))

# se llaman las funciones
lista_1(frase, lista, largo)
lista_invertida(lista)
