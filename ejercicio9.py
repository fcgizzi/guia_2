#!/usr/bin/env python
# -*- coding: utf-8 -*-

# función para crear matriz de identidad
def identidad(n, matriz, matrix):

    # se llena matriz con ceros
    for i in range(n):
        matriz.append([0]*n)

    # for que recorre matriz
    for i in range(n):
            for j in range(n):

                # cuando los indices sean iguales
                # el valor del elemento en matriz cambiará a 1

                if i == j:
                    matriz[i][j] = '1'

                # sino el elemento será 0

                else:
                   matriz[i][j] = '0'

                # se suma a la matrix el valor de la matriz con un espacio

                matrix += matriz[i][j] + ' '

            # además se le agrega un salto de linea

            matrix += '\n'

    # imprime
    print(matrix)

# valores:
# largo ingresado por usuario
# matriz vacía
# matrix vacía
n = int(input('Introducir largo: '))
matriz = []
matrix = ''

# se llama a la funcion
identidad(n, matriz, matrix)
