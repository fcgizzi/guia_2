#!/usr/bin/env python
# -*- coding: utf-8 -*-

# función
def recorrido_listas(lista, k, lista_mayores, lista_menores, lista_iguales, lista_multiplos):
    # ciclo
    # recorre lista para evaluar cada número
    for i in lista:

        # si encuentra numeros mayores a k los agrega en la lista de mayores,
        # y así con las siguientes evaluaciones

        if i > k:
            lista_mayores.append(i)
        if i < k:
            lista_menores.append(i)
        if i == k:
            lista_iguales.append(i)
        if (i % k) == 0:

        # (i % k) es el resto de la división entre el numero de la lista y k
        # por lo que debe ser 0 para que se considere multiplo

            lista_multiplos.append(i)

# variables:
# lista con los números enteros
# número ingresado por usuario
# listas vacías

lista = [4, 5, 9, 16, 39, 16, 30, 6, 1, 2, 80, 7, 20, 14, 26, 17, 3, 12]
k = int(input("Ingresar numero: "))
lista_mayores = []
lista_menores = []
lista_iguales = []
lista_multiplos = []

# llama a la funcón
recorrido_listas(lista, k, lista_mayores, lista_menores, lista_iguales, lista_multiplos)

# imprime listas
print("Lista de numeros: ", lista)
print("Mayores: ", lista_mayores)
print("Menores: ", lista_menores)
print("Iguales: ", lista_iguales)
print("Multiplos: ", lista_multiplos)
