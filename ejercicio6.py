#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

# funcion para contar repetidos
def repetidos(lista, i):

    # lista temporal
    temp=[]

    # se evalua cada sublista
    # creando una nueva lista 

    for palabra in lista[i]:
        temp.append(palabra)
    
    # contadores temporales
    mas = 1
    count = 1

    # lista temporal con las palabras

    for i in range(len(temp)):

        # se cuentan las palabras repetidas
        repeticion = temp.count(temp[i])

        # aumenta contador count cuando es superado por 
        # palabra repetida más de dos veces
        if repeticion > count:
            count += 1

        # ciclo que evalúa cada palabra y sus repeticiones

        while repeticion > mas:

            # se iguala al contador y se vuelve a evaluar
            mas = repeticion
            repeticion = temp.count(temp[i])

            # se imprime y se termina para que no repita
            # el print de manera infinita
            print("Palabra más repetida:", temp[i], '(',repeticion,"veces",')')
            break
        
        # cuando el contador count no haya sido superado
        # no exite palabra repetida
        if count == 1:
            print("No hay palabra repetida")
        
        # se imprime cualquier palabra que no esté repetida
        # y se agrega un break para que no imprima más de una palabra
        if repeticion == 1:
            print("Una de las palabra menos repetida:", temp[i])
            break


# función eliminadora de palabras repetidas
def eliminar(lista, i):
    
    # lista temporal vacía
    temp = []
    
    # se pasan las palabras a la lista temporal
    for palabra in lista[i]:
        temp.append(palabra)
        # ~ print(palabra)
    # ~ print(temp)
    
    # se invierte la lista para no borrar el primer elemento repetido
    temp.reverse()
    
    # se evalua la invertida
    for i in range(len(temp)):

        # conteo de palabras repetidas
        repeticion = temp.count(temp[i])
        # ~ print(repeticion)
        
        # al evaluarse de manera inversa
        # se irán eliminando desde la última palabra
        # por lo que, cuando ya no supere al 2
        # esta no se eliminará
        # quedando solo la primera
        
        if repeticion >= 2:

            # a las eliminadas se las cambia por este simbolo
            temp[i] = '--'
    
    # se vuelve a invertir la lista
    # y se imprime
    temp.reverse()
    print("Lista con eliminados:", temp)

# función crea lista
def creacion_listas(largo, listas, palabritas, lista):

    # ciclo crea lista con litsas dentro de ella
    # en estas se insertarán palabras de manera
    # aleatoria, extraidas de la lista con palabras

    for i in range(listas):
        lista.append([])
        for j in range(largo):
            lista[i].append(random.choice(palabritas))

    # variable n para enumerar listas
    n = 1

    # ciclo imprime listas y llama funciones evaluadoras

    for i in range(listas):
        print("\nlista", n, ":", lista[i])
        eliminar(lista, i)
        repetidos(lista, i)
        n += 1

# variables
# lista vacia
# largos de listas y sublistas (aleatorios)
#lista con plabras
lista = []
listas = random.randrange(2,5)
largo = random.randrange(3,8)
palabritas = ['trululu', 'pal', 'que', 'lea', 'the', 'game']
# ~ print(listas, largo)

# se llama la función
creacion_listas(largo, listas, palabritas, lista)
