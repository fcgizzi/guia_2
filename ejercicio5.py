#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

# función para modificar la lista editada
def excepciones(edit, final):

    # ciclo recorre lista editada
    for i in edit:

        # busca pares y numeros que no teminen en 0
        # los cambia por numeros aleatorios
        # agregando en la lista final

        if i % 2 == 0 and i % 10 != 0:
            final.append(random.randrange(1,21))

        # de lo contrario se agrega el mismo numero
        # a la lista final

        else:
            final.append(i)

    #imprime

    print("\n Lista final:", final)


# función que llena lista edit con valores no repetidos
def lista_editada(conjunto, edit):
    
    # ciclo que evalúa la lista conjunto
    
    for i in conjunto:
        if i not in edit:

        # condición; si el valor de edit no se 
        # encuentra en conjunto se agrega en edit, 
        # sin repetirse

            edit.append(i)
    print("\n Lista sin repetidos:", edit)

# función que concatena todas las listas
def concatenar(lista, conjunto):

    # recorre una lista con las 3 listas ya hechas
    for i in range (3):

        # se irán sumando en conjunto

        conjunto += lista[i]
    print("\n Concatenación:",conjunto)

# función para crear lista con 3 sublistas
def lista_a(largo):
    for i in range (3):
        lista.append([])
        for j in range (largo):
            
            # en cada lista se agregará una cantidad aleatoria
            # de numeros aleatorios
            
            lista[i].append(random.randrange(1, 51))
        print("Lista",i + 1,":",lista[i])

# variables
# listas vacías
# largo aleatorio de las sublistas
conjunto = []
lista =[]
largo = random.randrange(3,10)
edit = []
final = []

#se llaman las funciones
lista_a(largo)
concatenar(lista, conjunto)
lista_editada(conjunto, edit)
excepciones(edit, final)



