#!/usr/bin/env python
# -*- coding: utf-8 -*-

# función para desencriptar mensaje
def desencriptado(mensaje, mensaje_nuevo):
    
    # for que recorre el mensaje como lista
    
    for i in mensaje:

    # si encuentra un elemento distinto de 'X' en 
    # la lista se agrega en la lista nueva

        if i != 'X':
            mensaje_nuevo.append(i)
    
    # se imprimen ambos mensajes
    # mensaje nuevo se vuelve a convertir en un 
    # arreglo en la linea 23

    print("mensaje encriptado:", encriptado)
    cadena = "".join(mensaje_nuevo)
    print("mensaje desencriptado:", cadena)

# variables: 
# mensaje encriptado
# mensaje transformado en lista
# mensaje (lista) invertida
# mensaje nuevo que se rellenará

encriptado = "!XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"
mensaje = list(encriptado)
mensaje.reverse()
mensaje_nuevo = []

# se llama a la funcón
desencriptado(mensaje, mensaje_nuevo)
