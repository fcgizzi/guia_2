#!/usr/bin/env python
# -*- coding: utf-8 -*-

# biblioteca random
import random

# función que invierte las filas de la matriz
def reversa(matriz, n):
    
    # inviere lista que contiene sublistas
    matriz.reverse()
    
    # se imprime cada sublista
    # estando esta ya inveertida
    for i in range (n):
        print(matriz[i])

# función crea matriz
def crear_matriz(n, matriz):
    
    # crea lsita con sublistas de tamaño n
    for i in range (n):
        
        # de manera vacía
        matriz.append([])
        for j in range (n):
            
            # a cada i se le da un valor aleatorio
            matriz[i].append(random.randrange(1, 21))
        
        # imprime matriz
        print(matriz[i])

# variables
# tamaño y lista vacía
n = int(input("Ingresar tamaño matriz: "))
matriz = []

# se llaman las funciones
print('\n Original: \n')
crear_matriz(n, matriz)

print('\n Intercambiadas: \n')
reversa(matriz, n)

